using System.Collections;
using System.Collections.Generic;
using strange.extensions.mediation.impl;
using UnityEngine;

public class UIMainMediator : Mediator
{
    [Inject] public UIMainView View { get; set; }

    public override void OnRegister()
    {
        base.OnRegister();
        Init();
    }

    private void Init()
    {
        View.GetComponent<Canvas>().worldCamera = ReferenceManager.Instance.Camera;
    }
}
