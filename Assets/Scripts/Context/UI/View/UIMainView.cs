using System.Collections;
using System.Collections.Generic;
using strange.extensions.mediation.impl;
using UnityEngine;

public class UIMainView : View
{
    [SerializeField] public PrimitivePanelView PrimitivePanelView;
    [SerializeField] public GameObject HierarchyPanel;

    [SerializeField] public HierarchyElementView HighlightedElement;
    [SerializeField] public GameObject FrameableWindow;
}
