using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using ProceduralPrimitivesUtil;
using strange.extensions.mediation.impl;
using UnityEditor;
using UnityEngine;
using UnityEngine.AddressableAssets;
using UnityEngine.ResourceManagement.AsyncOperations;
using Object = System.Object;
using Random = UnityEngine.Random;

public class PrimitivePanelMediator : Mediator
{
    [Inject] public PrimitivePanelView View { get; set; }

    public override void OnRegister()
    {
        View.BoxButton.onClick.AddListener(Box);
        View.ConeButton.onClick.AddListener(Cone);
        View.RingButton.onClick.AddListener(Ring);
        View.RectTubeButton.onClick.AddListener(RectTube);
        View.CylinderButton.onClick.AddListener(Cylinder);
        View.SphereButton.onClick.AddListener(Sphere);
        View.PyramidButton.onClick.AddListener(Pyramid);
        View.TorusButton.onClick.AddListener(Torus);
    }

    private void Torus()
    {
        CreatePrimitive(ProceduralPrimitiveType.Torus);
    }

    private void Sphere()
    {
        CreatePrimitive(ProceduralPrimitiveType.Sphere);
    }

    private void RectTube()
    {
        CreatePrimitive(ProceduralPrimitiveType.RectTube);
    }

    private void Ring()
    {
        CreatePrimitive(ProceduralPrimitiveType.Ring);
    }

    private void Pyramid()
    {
        CreatePrimitive(ProceduralPrimitiveType.Pyramid);
    }

    private void Cylinder()
    {
        CreatePrimitive(ProceduralPrimitiveType.Cylinder);
    }

    private void Cone()
    {
        CreatePrimitive(ProceduralPrimitiveType.Cone);
    }

    public override void OnRemove()
    {
        View.BoxButton.onClick.RemoveListener(Box);
        View.ConeButton.onClick.RemoveListener(Cone);
        View.RingButton.onClick.RemoveListener(Ring);
        View.RectTubeButton.onClick.RemoveListener(RectTube);
        View.CylinderButton.onClick.RemoveListener(Cylinder);
        View.SphereButton.onClick.RemoveListener(Sphere);
        View.PyramidButton.onClick.RemoveListener(Pyramid);
        View.TorusButton.onClick.RemoveListener(Torus);
    }

    private void Box()
    {
        CreatePrimitive(ProceduralPrimitiveType.Box);
    }

    private void CreatePrimitive(ProceduralPrimitiveType type)
    {
        GameObject obj = ProceduralPrimitives.CreatePrimitive(type);
        obj.transform.parent = PlaygroundManager.Instance.transform;
        obj.name = $"{type + "_" + StatisticsManager.Instance.GetPrimitiveCounterByType(type)}";
        
        
        AddComponents(obj, type);
        AddShader(obj);

        PrimitiveView primitiveView = obj.GetComponent<PrimitiveView>();
        
        PlaygroundManager.Instance.Objects.Add(primitiveView);
        PlaygroundManager.Instance.PrimaryObject = primitiveView;
        PlaygroundManager.Instance.ChangePrimaryObject();
        PlaygroundManager.Instance.AddHierarchyElement();
    }

    private void AddComponents(GameObject obj, ProceduralPrimitiveType type)
    {
        obj.AddComponent<MeshCollider>().convex = true;
        PrimitiveView primitiveView = obj.AddComponent<PrimitiveView>();
        primitiveView.Type = type;
        primitiveView.ID = Guid.NewGuid().ToString();
    }

    private void AddShader(GameObject obj)
    {
        Shader sh = Resources.Load<Shader>("Shaders/PrimitiveShader");
        Renderer renderer = obj.GetComponent<Renderer>();
        renderer.material.shader = sh;
        Color mainColor = new Color(Random.Range(0.7f,1), Random.Range(0.7f,1), Random.Range(0.7f,1), 1);
        Color outlineColor = new Color(Random.Range(0.7f,1), Random.Range(0.7f,1), Random.Range(0.7f,1), 1);
        renderer.material.SetColor("_MainColor", mainColor);
        renderer.material.SetColor("_OutlineColor", outlineColor);
    }
}
