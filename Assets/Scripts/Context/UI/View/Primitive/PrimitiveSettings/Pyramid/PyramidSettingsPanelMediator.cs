using System.Collections;
using System.Collections.Generic;
using ProceduralPrimitivesUtil;
using strange.extensions.mediation.impl;
using TMPro;
using UnityEngine;

public class PyramidSettingsPanelMediator : Mediator
{
    [Inject] public PyramidSettingsPanelView View { get; set; }
    
    private Pyramid MainObject;
    
    public override void OnRegister()
    {
        base.OnRegister();
        Init();
        Binding();
    }

    public override void OnRemove()
    {
        base.OnRemove();
        UnBinding();
    }

    private void UnBinding()
    {
        View.PositionX.onValueChanged.RemoveListener(PositionXChange);
        View.PositionY.onValueChanged.RemoveListener(PositionYChange);
        View.PositionZ.onValueChanged.RemoveListener(PositionZChange);
        
        View.RotationX.onValueChanged.RemoveListener(RotationXChange);
        View.RotationY.onValueChanged.RemoveListener(RotationYChange);
        View.RotationZ.onValueChanged.RemoveListener(RotationZChange);
        
        View.ScaleX.onValueChanged.RemoveListener(ScaleXChange);
        View.ScaleY.onValueChanged.RemoveListener(ScaleYChange);
        View.ScaleZ.onValueChanged.RemoveListener(ScaleZChange);
        
        View.Length1.onValueChanged.RemoveListener(Length1Change);
        View.Length2.onValueChanged.RemoveListener(Length2Change);
        View.Width1.onValueChanged.RemoveListener(Width1Change);
        View.Width2.onValueChanged.RemoveListener(Width2Change);
        View.Height.onValueChanged.RemoveListener(HeightChange);

        View.UVOffsetU.onValueChanged.RemoveListener(UVOffsetUChange);
        View.UVOffsetV.onValueChanged.RemoveListener(UVOffsetVChange);
        
        View.UVTillingU.onValueChanged.RemoveListener(UVTillingUChange);
        View.UVTillingV.onValueChanged.RemoveListener(UVTillingVChange);
        
        View.FlipNormals.onValueChanged.RemoveListener(FlipNormalsChange);
        View.Matereial.onValueChanged.RemoveListener(MaterialChange);
        View.Metallic.onValueChanged.RemoveListener(MetallicChange);
        View.Smoothness.onValueChanged.RemoveListener(SmoothnessChange);
        View.Opacity.onValueChanged.RemoveListener(OpacityChange);
    }

    private void Binding()
    {
        View.PositionX.onValueChanged.AddListener(PositionXChange);
        View.PositionY.onValueChanged.AddListener(PositionYChange);
        View.PositionZ.onValueChanged.AddListener(PositionZChange);
        
        View.RotationX.onValueChanged.AddListener(RotationXChange);
        View.RotationY.onValueChanged.AddListener(RotationYChange);
        View.RotationZ.onValueChanged.AddListener(RotationZChange);
        
        View.ScaleX.onValueChanged.AddListener(ScaleXChange);
        View.ScaleY.onValueChanged.AddListener(ScaleYChange);
        View.ScaleZ.onValueChanged.AddListener(ScaleZChange);
        
        View.Length1.onValueChanged.AddListener(Length1Change);
        View.Length2.onValueChanged.AddListener(Length2Change);
        View.Width1.onValueChanged.AddListener(Width1Change);
        View.Width2.onValueChanged.AddListener(Width2Change);
        View.Height.onValueChanged.AddListener(HeightChange);

        View.UVOffsetU.onValueChanged.AddListener(UVOffsetUChange);
        View.UVOffsetV.onValueChanged.AddListener(UVOffsetVChange);
        
        View.UVTillingU.onValueChanged.AddListener(UVTillingUChange);
        View.UVTillingV.onValueChanged.AddListener(UVTillingVChange);
        
        View.FlipNormals.onValueChanged.AddListener(FlipNormalsChange);
        View.Matereial.onValueChanged.AddListener(MaterialChange);
        View.Metallic.onValueChanged.AddListener(MetallicChange);
        View.Smoothness.onValueChanged.AddListener(SmoothnessChange);
        View.Opacity.onValueChanged.AddListener(OpacityChange);
    }

    private void OpacityChange(float arg0)
    {
        MainObject.GetComponent<Renderer>().material.SetFloat("_Opacity", arg0);
    }

    private void HeightChange(string arg0)
    {
        MainObject.height = float.Parse(arg0);
        MainObject.Apply();
    }
    
    private void Width2Change(string arg0)
    {
        MainObject.width2 = float.Parse(arg0);
        MainObject.Apply();
    }
    
    private void Width1Change(string arg0)
    {
        MainObject.width1 = float.Parse(arg0);
        MainObject.Apply();
    }
    
    private void Length2Change(string arg0)
    {
        MainObject.length2 = float.Parse(arg0);
        MainObject.Apply();
    }
    
    private void Length1Change(string arg0)
    {
        MainObject.length1 = float.Parse(arg0);
        MainObject.Apply();
    }

    private void SmoothnessChange(float arg0)
    {
        MainObject.GetComponent<Renderer>().material.SetFloat("_Smoothness", arg0);
    }

    private void MetallicChange(float arg0)
    {
        MainObject.GetComponent<Renderer>().material.SetFloat("_Metallic", arg0);
    }

    private void MaterialChange(int arg0)
    {
        PrimitiveMaterialReference reference = ReferenceManager.Instance.Materials[arg0];
        MainObject.GetComponent<Renderer>().material.SetColor("_MainColor", Color.white);
        MainObject.GetComponent<Renderer>().material.SetTexture("_MainTexture", reference.Albdeo);
        MainObject.GetComponent<Renderer>().material.SetTexture("_NormalMap", reference.Normal);
    }

    private void FlipNormalsChange(bool arg0)
    {
        MainObject.flipNormals = arg0;
        MainObject.Apply();
    }

    private void UVTillingVChange(string arg0)
    {
        MainObject.UVTiling = new Vector2(MainObject.UVTiling.x, float.Parse(arg0));
        MainObject.Apply();
    }
    
    private void UVTillingUChange(string arg0)
    {
        MainObject.UVTiling = new Vector2(float.Parse(arg0), MainObject.UVTiling.y);
        MainObject.Apply();
    }

    private void UVOffsetVChange(string arg0)
    {
        MainObject.UVOffset = new Vector2(MainObject.UVOffset.x, float.Parse(arg0));
        MainObject.Apply();
    }
    
    private void UVOffsetUChange(string arg0)
    {
        MainObject.UVOffset = new Vector2(float.Parse(arg0), MainObject.UVOffset.y);
        MainObject.Apply();
    }
    
    private void ScaleZChange(string arg0)
    {
        MainObject.transform.localScale =
            new Vector3(MainObject.transform.localScale.x, MainObject.transform.localScale.y, float.Parse(arg0));
        MainObject.Apply();
    }
    
    private void ScaleYChange(string arg0)
    {
        MainObject.transform.localScale =
            new Vector3(MainObject.transform.localScale.x, float.Parse(arg0), MainObject.transform.localScale.z);
        MainObject.Apply();
    }
    
    private void ScaleXChange(string arg0)
    {
        MainObject.transform.localScale =
            new Vector3(float.Parse(arg0), MainObject.transform.localScale.y, MainObject.transform.localScale.z);
        MainObject.Apply();
    }

    private void RotationZChange(string arg0)
    {
        MainObject.transform.rotation = Quaternion.Euler(MainObject.transform.rotation.eulerAngles.x, MainObject.transform.rotation.eulerAngles.y, float.Parse(arg0));
        MainObject.Apply();
    }
    
    private void RotationYChange(string arg0)
    {
        MainObject.transform.rotation = Quaternion.Euler(MainObject.transform.rotation.eulerAngles.x, float.Parse(arg0), MainObject.transform.rotation.eulerAngles.z);
        MainObject.Apply();
    }
    
    private void RotationXChange(string arg0)
    {
        MainObject.transform.rotation =  Quaternion.Euler(float.Parse(arg0), MainObject.transform.rotation.eulerAngles.y, MainObject.transform.rotation.eulerAngles.z);
        MainObject.Apply();
    }

    private void PositionZChange(string arg0)
    {
        MainObject.transform.position = new Vector3(MainObject.transform.position.x, MainObject.transform.position.y, float.Parse(arg0));
        MainObject.Apply();
    }
    
    private void PositionYChange(string arg0)
    {
        MainObject.transform.position = new Vector3(MainObject.transform.position.x, float.Parse(arg0), MainObject.transform.position.z);
        MainObject.Apply();
    }

    private void PositionXChange(string arg0)
    {
        MainObject.transform.position = new Vector3(float.Parse(arg0), MainObject.transform.position.y, MainObject.transform.position.z);
        MainObject.Apply();
    }

    private void Init()
    {
        MainObject = PlaygroundManager.Instance.PrimaryObject.GetComponent<Pyramid>();

        View.PositionX.text = MainObject.transform.position.x.ToString();
        View.PositionY.text = MainObject.transform.position.y.ToString();
        View.PositionZ.text = MainObject.transform.position.z.ToString();

        View.RotationX.text = MainObject.rotation.x.ToString();
        View.RotationY.text = MainObject.rotation.y.ToString();
        View.RotationZ.text = MainObject.rotation.z.ToString();

        View.ScaleX.text = MainObject.transform.localScale.x.ToString();
        View.ScaleY.text = MainObject.transform.localScale.y.ToString();
        View.ScaleZ.text = MainObject.transform.localScale.z.ToString();

        View.Length1.text = MainObject.length1.ToString();
        View.Length2.text = MainObject.length2.ToString();
        View.Width1.text = MainObject.width1.ToString();
        View.Width2.text = MainObject.width2.ToString();
        View.Height.text = MainObject.height.ToString();

        View.UVOffsetU.text = MainObject.UVOffset.x.ToString();
        View.UVOffsetV.text = MainObject.UVOffset.y.ToString();

        View.UVTillingU.text = MainObject.UVTiling.x.ToString();
        View.UVTillingV.text = MainObject.UVTiling.y.ToString();

        View.FlipNormals.isOn = MainObject.flipNormals;
        

        View.Matereial.ClearOptions();

        foreach (var item in ReferenceManager.Instance.Materials)
        {
            View.Matereial.options.Add(new TMP_Dropdown.OptionData()
            {
                text = item.ID
            });
        }

        View.Metallic.value = MainObject.GetComponent<Renderer>().material.GetFloat("_Metallic");
        View.Smoothness.value = MainObject.GetComponent<Renderer>().material.GetFloat("_Smoothness");

        MainObject.Apply();
    }
}
