using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class SynthethicSettingsPanelView : PrimitiveSettingsView
{
    [SerializeField] public TMP_InputField PositionX;
    [SerializeField] public TMP_InputField PositionY;
    [SerializeField] public TMP_InputField PositionZ;
    [Space(10)]
    [SerializeField] public TMP_InputField RotationX;
    [SerializeField] public TMP_InputField RotationY;
    [SerializeField] public TMP_InputField RotationZ;
    [Space(10)]
    [SerializeField] public TMP_InputField ScaleX;
    [SerializeField] public TMP_InputField ScaleY;
    [SerializeField] public TMP_InputField ScaleZ;
    
    [Space(10)]
    [SerializeField] public TMP_InputField UVOffsetU;
    [SerializeField] public TMP_InputField UVOffsetV;
    [Space(10)]
    [SerializeField] public TMP_InputField UVTillingU;
    [SerializeField] public TMP_InputField UVTillingV;
    [Space(10)]
    [SerializeField] public Toggle FlipNormals;

    [Space(10)] [SerializeField] public Slider Metallic;
    [SerializeField] public Slider Smoothness;
    [SerializeField] public TMP_Dropdown Matereial;
    [SerializeField] public Slider Opacity;
}
