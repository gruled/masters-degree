using System.Collections;
using System.Collections.Generic;
using ProceduralPrimitivesUtil;
using strange.extensions.mediation.impl;
using TMPro;
using UnityEngine;

public class TorusSettingsPanelMediator : Mediator
{
    [Inject] public TorusSettingsPanelView View { get; set; }
    
    private Torus MainObject;
    
    public override void OnRegister()
    {
        base.OnRegister();
        Init();
        Binding();
    }

    public override void OnRemove()
    {
        base.OnRemove();
        UnBinding();
    }

    private void UnBinding()
    {
        View.PositionX.onValueChanged.RemoveListener(PositionXChange);
        View.PositionY.onValueChanged.RemoveListener(PositionYChange);
        View.PositionZ.onValueChanged.RemoveListener(PositionZChange);
        
        View.RotationX.onValueChanged.RemoveListener(RotationXChange);
        View.RotationY.onValueChanged.RemoveListener(RotationYChange);
        View.RotationZ.onValueChanged.RemoveListener(RotationZChange);
        
        View.ScaleX.onValueChanged.RemoveListener(ScaleXChange);
        View.ScaleY.onValueChanged.RemoveListener(ScaleYChange);
        View.ScaleZ.onValueChanged.RemoveListener(ScaleZChange);
        
        View.Radius1.onValueChanged.RemoveListener(Radius1Change);
        View.Radius2.onValueChanged.RemoveListener(Radius2Change);

        View.UVOffsetU.onValueChanged.RemoveListener(UVOffsetUChange);
        View.UVOffsetV.onValueChanged.RemoveListener(UVOffsetVChange);
        
        View.UVTillingU.onValueChanged.RemoveListener(UVTillingUChange);
        View.UVTillingV.onValueChanged.RemoveListener(UVTillingVChange);
        
        View.FlipNormals.onValueChanged.RemoveListener(FlipNormalsChange);
        View.Matereial.onValueChanged.RemoveListener(MaterialChange);
        View.Metallic.onValueChanged.RemoveListener(MetallicChange);
        View.Smoothness.onValueChanged.RemoveListener(SmoothnessChange);
        View.Opacity.onValueChanged.RemoveListener(OpacityChange);
    }

    private void Binding()
    {
        View.PositionX.onValueChanged.AddListener(PositionXChange);
        View.PositionY.onValueChanged.AddListener(PositionYChange);
        View.PositionZ.onValueChanged.AddListener(PositionZChange);
        
        View.RotationX.onValueChanged.AddListener(RotationXChange);
        View.RotationY.onValueChanged.AddListener(RotationYChange);
        View.RotationZ.onValueChanged.AddListener(RotationZChange);
        
        View.ScaleX.onValueChanged.AddListener(ScaleXChange);
        View.ScaleY.onValueChanged.AddListener(ScaleYChange);
        View.ScaleZ.onValueChanged.AddListener(ScaleZChange);
        
        View.Radius1.onValueChanged.AddListener(Radius1Change);
        View.Radius2.onValueChanged.AddListener(Radius2Change);

        View.UVOffsetU.onValueChanged.AddListener(UVOffsetUChange);
        View.UVOffsetV.onValueChanged.AddListener(UVOffsetVChange);
        
        View.UVTillingU.onValueChanged.AddListener(UVTillingUChange);
        View.UVTillingV.onValueChanged.AddListener(UVTillingVChange);
        
        View.FlipNormals.onValueChanged.AddListener(FlipNormalsChange);
        View.Matereial.onValueChanged.AddListener(MaterialChange);
        View.Metallic.onValueChanged.AddListener(MetallicChange);
        View.Smoothness.onValueChanged.AddListener(SmoothnessChange);
        View.Opacity.onValueChanged.AddListener(OpacityChange);
    }

    private void OpacityChange(float arg0)
    {
        MainObject.GetComponent<Renderer>().material.SetFloat("_Opacity", arg0);
    }

    private void Radius2Change(string arg0)
    {
        MainObject.radius2 = float.Parse(arg0);
        MainObject.Apply();
    }
    
    private void Radius1Change(string arg0)
    {
        MainObject.radius1 = float.Parse(arg0);
        MainObject.Apply();
    }

    private void SmoothnessChange(float arg0)
    {
        MainObject.GetComponent<Renderer>().material.SetFloat("_Smoothness", arg0);
    }

    private void MetallicChange(float arg0)
    {
        MainObject.GetComponent<Renderer>().material.SetFloat("_Metallic", arg0);
    }

    private void MaterialChange(int arg0)
    {
        PrimitiveMaterialReference reference = ReferenceManager.Instance.Materials[arg0];
        MainObject.GetComponent<Renderer>().material.SetColor("_MainColor", Color.white);
        MainObject.GetComponent<Renderer>().material.SetTexture("_MainTexture", reference.Albdeo);
        MainObject.GetComponent<Renderer>().material.SetTexture("_NormalMap", reference.Normal);
    }

    private void FlipNormalsChange(bool arg0)
    {
        MainObject.flipNormals = arg0;
        MainObject.Apply();
    }

    private void UVTillingVChange(string arg0)
    {
        MainObject.UVTiling = new Vector2(MainObject.UVTiling.x, float.Parse(arg0));
        MainObject.Apply();
    }
    
    private void UVTillingUChange(string arg0)
    {
        MainObject.UVTiling = new Vector2(float.Parse(arg0), MainObject.UVTiling.y);
        MainObject.Apply();
    }

    private void UVOffsetVChange(string arg0)
    {
        MainObject.UVOffset = new Vector2(MainObject.UVOffset.x, float.Parse(arg0));
        MainObject.Apply();
    }
    
    private void UVOffsetUChange(string arg0)
    {
        MainObject.UVOffset = new Vector2(float.Parse(arg0), MainObject.UVOffset.y);
        MainObject.Apply();
    }
    
    private void ScaleZChange(string arg0)
    {
        MainObject.transform.localScale =
            new Vector3(MainObject.transform.localScale.x, MainObject.transform.localScale.y, float.Parse(arg0));
        MainObject.Apply();
    }
    
    private void ScaleYChange(string arg0)
    {
        MainObject.transform.localScale =
            new Vector3(MainObject.transform.localScale.x, float.Parse(arg0), MainObject.transform.localScale.z);
        MainObject.Apply();
    }
    
    private void ScaleXChange(string arg0)
    {
        MainObject.transform.localScale =
            new Vector3(float.Parse(arg0), MainObject.transform.localScale.y, MainObject.transform.localScale.z);
        MainObject.Apply();
    }

    private void RotationZChange(string arg0)
    {
        MainObject.transform.rotation = Quaternion.Euler(MainObject.transform.rotation.eulerAngles.x, MainObject.transform.rotation.eulerAngles.y, float.Parse(arg0));
        MainObject.Apply();
    }
    
    private void RotationYChange(string arg0)
    {
        MainObject.transform.rotation = Quaternion.Euler(MainObject.transform.rotation.eulerAngles.x, float.Parse(arg0), MainObject.transform.rotation.eulerAngles.z);
        MainObject.Apply();
    }
    
    private void RotationXChange(string arg0)
    {
        MainObject.transform.rotation =  Quaternion.Euler(float.Parse(arg0), MainObject.transform.rotation.eulerAngles.y, MainObject.transform.rotation.eulerAngles.z);
        MainObject.Apply();
    }

    private void PositionZChange(string arg0)
    {
        MainObject.transform.position = new Vector3(MainObject.transform.position.x, MainObject.transform.position.y, float.Parse(arg0));
        MainObject.Apply();
    }
    
    private void PositionYChange(string arg0)
    {
        MainObject.transform.position = new Vector3(MainObject.transform.position.x, float.Parse(arg0), MainObject.transform.position.z);
        MainObject.Apply();
    }

    private void PositionXChange(string arg0)
    {
        MainObject.transform.position = new Vector3(float.Parse(arg0), MainObject.transform.position.y, MainObject.transform.position.z);
        MainObject.Apply();
    }

    private void Init()
    {
        MainObject = PlaygroundManager.Instance.PrimaryObject.GetComponent<Torus>();

        View.PositionX.text = MainObject.transform.position.x.ToString();
        View.PositionY.text = MainObject.transform.position.y.ToString();
        View.PositionZ.text = MainObject.transform.position.z.ToString();

        View.RotationX.text = MainObject.rotation.x.ToString();
        View.RotationY.text = MainObject.rotation.y.ToString();
        View.RotationZ.text = MainObject.rotation.z.ToString();

        View.ScaleX.text = MainObject.transform.localScale.x.ToString();
        View.ScaleY.text = MainObject.transform.localScale.y.ToString();
        View.ScaleZ.text = MainObject.transform.localScale.z.ToString();

        View.Radius1.text = MainObject.radius1.ToString();
        View.Radius2.text = MainObject.radius2.ToString();

        View.UVOffsetU.text = MainObject.UVOffset.x.ToString();
        View.UVOffsetV.text = MainObject.UVOffset.y.ToString();

        View.UVTillingU.text = MainObject.UVTiling.x.ToString();
        View.UVTillingV.text = MainObject.UVTiling.y.ToString();

        View.FlipNormals.isOn = MainObject.flipNormals;
        

        View.Matereial.ClearOptions();

        foreach (var item in ReferenceManager.Instance.Materials)
        {
            View.Matereial.options.Add(new TMP_Dropdown.OptionData()
            {
                text = item.ID
            });
        }

        View.Metallic.value = MainObject.GetComponent<Renderer>().material.GetFloat("_Metallic");
        View.Smoothness.value = MainObject.GetComponent<Renderer>().material.GetFloat("_Smoothness");

        MainObject.Apply();
    }
}
