using System.Collections;
using System.Collections.Generic;
using strange.extensions.mediation.impl;
using UnityEngine;
using UnityEngine.UI;

public class PrimitivePanelView : View
{
    [SerializeField] public Button BoxButton;
    [SerializeField] public Button RingButton;
    [SerializeField] public Button CylinderButton;
    [SerializeField] public Button PyramidButton;
    [SerializeField] public Button ConeButton;
    [SerializeField] public Button RectTubeButton;
    [SerializeField] public Button SphereButton;
    [SerializeField] public Button TorusButton;
}
