using System.Collections;
using System.Collections.Generic;
using strange.extensions.mediation.impl;
using UnityEngine;

public class MeshStatisticsMediator : Mediator
{
    [Inject] public MeshStatisticsView View { get; set; }

    public override void OnRemove()
    {
        base.OnRemove();
        BackendManager.Instance.OnSelectObject -= SelectObject;
    }

    public override void OnRegister()
    {
        base.OnRegister();
        StartCoroutine(DelayedRegister());
    }

    private IEnumerator DelayedRegister()
    {
        yield return new WaitWhile(GetInstanceStatus);
        BackendManager.Instance.OnSelectObject += SelectObject;
    }

    private bool GetInstanceStatus()
    {
        if (BackendManager.Instance)
        {
            return true;
        }

        return false;
    }

    private void SelectObject()
    {
        MeshFilter obj = PlaygroundManager.Instance.PrimaryObject.GetComponent<MeshFilter>();
        View.Text.SetText($"Вершин: {obj.mesh.vertexCount}\nТреугольников: {obj.mesh.triangles.Length}");
    }
}
