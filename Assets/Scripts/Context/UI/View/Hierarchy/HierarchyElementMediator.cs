using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using strange.extensions.mediation.impl;
using UnityEngine;
using UnityEngine.UI;

public class HierarchyElementMediator : Mediator
{
    [Inject] public HierarchyElementView View { get; set; }

    public override void OnRegister()
    {
        base.OnRegister();
        UIManager.Instance.UnHighlightHierarchyElement();
        View.DeleteButton.onClick.AddListener(Delete);
        View.Name.transform.parent.GetComponent<Button>().onClick.AddListener(SelectObject);
        UIManager.Instance.HighlightHierarchyElement(View);
    }

    private void SelectObject()
    {
        if (PlaygroundManager.Instance.Objects.Any(x => x.ID.Equals(View.ID)))
        {
            UIManager.Instance.UnHighlightHierarchyElement();
            PlaygroundManager.Instance.UnHighlightObject();
            PlaygroundManager.Instance.PrimaryObject = PlaygroundManager.Instance.Objects.First(x => x.ID.Equals(View.ID));
            PlaygroundManager.Instance.ChangePrimaryObject();
            PlaygroundManager.Instance.HighlightObject();
            UIManager.Instance.HighlightHierarchyElement(View);
            BackendManager.Instance.SelectObject();
        }
    }

    private void Delete()
    {
        PrimitiveView primitiveView = PlaygroundManager.Instance.Objects.First(x => x.ID.Equals(View.ID));
        PlaygroundManager.Instance.Objects.Remove(primitiveView);
        if (PlaygroundManager.Instance.PrimaryObject == primitiveView)
        {
            PlaygroundManager.Instance.PrimaryObject = null;
            UIManager.Instance.ClearPrimitiveSettingsPanel();
        }
        Destroy(primitiveView.gameObject);
        Destroy(this.gameObject);
    }
}
