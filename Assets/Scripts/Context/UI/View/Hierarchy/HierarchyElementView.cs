using System.Collections;
using System.Collections.Generic;
using strange.extensions.mediation.impl;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class HierarchyElementView : View
{
    [SerializeField] public string ID;
    [SerializeField] public TextMeshProUGUI Name;
    [SerializeField] public Button DeleteButton;
    [SerializeField] public Image Background;
}
