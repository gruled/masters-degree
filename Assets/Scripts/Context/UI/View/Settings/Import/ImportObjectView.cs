using System.Collections;
using System.Collections.Generic;
using strange.extensions.mediation.impl;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class ImportObjectView : View
{
    [SerializeField] public TMP_InputField Adress;
    [SerializeField] public Button ImportButton;
}
