using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using Dummiesman;
using ProceduralPrimitivesUtil;
using strange.extensions.mediation.impl;
using UnityEngine;
using UnityEngine.Networking;
using UnityEngine.UI;
using Random = UnityEngine.Random;

public class ImportObjectMediator : Mediator
{
    [Inject] public ImportObjectView View { get; set; }

    public override void OnRemove()
    {
        base.OnRemove();
        View.ImportButton.onClick.RemoveListener(Import);
    }

    public override void OnRegister()
    {
        base.OnRegister();
        View.ImportButton.onClick.AddListener(Import);
    }

    private void Import()
    {
        StartCoroutine(DownloadCoroutine());
    }

    private IEnumerator DownloadCoroutine()
    {
        if (!String.IsNullOrEmpty(View.Adress.text))
        {
            string path = null;
#if UNITY_WEBGL
            path = Path.Combine(Application.absoluteURL, $"StreamingAssets/{View.Adress.text}");
#endif
#if UNITY_EDITOR_64
            path = Path.Combine(Application.streamingAssetsPath, $"{View.Adress.text}");
#endif

            var www = new WWW(path);
            while (!www.isDone)
            {
                yield return new WaitForSeconds(0.5f);
            }

            var textStream = new MemoryStream(Encoding.UTF8.GetBytes(www.text));
            var loadedObj = new OBJLoader().Load(textStream);
            

            foreach (var element in loadedObj.GetComponentsInChildren<Renderer>().ToList())
            {
                yield return new WaitForSeconds(0.5f);
                element.transform.parent = PlaygroundManager.Instance.transform;
                element.name = $"{element.name}";
                element.gameObject.AddComponent<MeshCollider>().convex = true;
                PrimitiveView primitiveView = element.gameObject.AddComponent<PrimitiveView>();
                primitiveView.Type = ProceduralPrimitiveType.Syntethic;
                primitiveView.ID = Guid.NewGuid().ToString();
                primitiveView.IsPrimitive = false;
                
                Shader sh = Resources.Load<Shader>("Shaders/PrimitiveShader");
                Renderer renderer = element;
                renderer.material.shader = sh;
                Color mainColor = new Color(Random.Range(0.7f,1), Random.Range(0.7f,1), Random.Range(0.7f,1), 1);
                Color outlineColor = new Color(Random.Range(0.7f,1), Random.Range(0.7f,1), Random.Range(0.7f,1), 1);
                renderer.material.SetColor("_MainColor", mainColor);
                renderer.material.SetColor("_OutlineColor", outlineColor);

                PlaygroundManager.Instance.Objects.Add(primitiveView);
                PlaygroundManager.Instance.PrimaryObject = primitiveView;
                PlaygroundManager.Instance.ChangePrimaryObject();
                PlaygroundManager.Instance.AddHierarchyElement();
                StartCoroutine(ReCalculate(element.GetComponent<MeshFilter>()));
                yield return new WaitForSeconds(0.3f);
            }
            Destroy(loadedObj.gameObject);
        }
    }

    private IEnumerator ReCalculate(MeshFilter meshFilter)
    {
        yield return new WaitForSeconds(0.1f);
        meshFilter.mesh.RecalculateNormals();
        meshFilter.mesh.RecalculateBounds();
        meshFilter.mesh.RecalculateTangents();
    }
}
