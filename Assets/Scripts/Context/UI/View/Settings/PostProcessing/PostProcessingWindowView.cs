using System.Collections;
using System.Collections.Generic;
using strange.extensions.mediation.impl;
using TMPro;
using UnityEngine;
using UnityEngine.Rendering.PostProcessing;
using UnityEngine.UI;

public class PostProcessingWindowView : View
{
    [SerializeField] public PostProcessVolume Volume;
    [Space]
    [SerializeField] public Button CloseButton;
    [SerializeField] public Slider Vignette;
    [SerializeField] public Slider ChromaticAbberation;
    [SerializeField] public Slider Bloom;
    [SerializeField] public TMP_Dropdown ColorGradingMode;
    [SerializeField] public Slider Temperature;
    [SerializeField] public Slider Tint;
    [SerializeField] public Slider Distortion;
    [SerializeField] public Slider Grain;
}
