using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using strange.extensions.mediation.impl;
using UnityEngine;
using UnityEngine.UI;

public class PostprocessingButtonMediator : Mediator
{
    [Inject] public PostprocessingButtonView View { get; set; }
    private Button PostProcessingButton;

    private void Awake()
    {
        Init();
    }

    public override void OnRemove()
    {
        base.OnRemove();
        PostProcessingButton.onClick.RemoveListener(PostProcessingButtonClick);
    }

    public override void OnRegister()
    {
        base.OnRegister();
        PostProcessingButton.onClick.AddListener(PostProcessingButtonClick);
    }

    private void PostProcessingButtonClick()
    {
        GameObjectReference reference =
            ReferenceManager.Instance.References.First(x => x.Type.Equals(ReferenceType.PostProcessingWindow));
        reference.Reference.InstantiateAsync(UIManager.Instance.UIMainView.FrameableWindow.transform);
    }

    private void Init()
    {
        PostProcessingButton = GetComponent<Button>();
    }
}
