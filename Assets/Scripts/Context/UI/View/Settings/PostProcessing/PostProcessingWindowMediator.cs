using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using strange.extensions.mediation.impl;
using UnityEngine;
using UnityEngine.Rendering.PostProcessing;
using UnityEngine.UI;

public class PostProcessingWindowMediator : Mediator
{
    [Inject] public PostProcessingWindowView View { get; set; }

    public override void OnRemove()
    {
        base.OnRemove();
        View.CloseButton.onClick.RemoveListener(Close);
        View.Vignette.onValueChanged.RemoveListener(VignetteChange);
        View.Bloom.onValueChanged.RemoveListener(BloomChange);
        View.Distortion.onValueChanged.RemoveListener(DistortionChange);
        View.Grain.onValueChanged.RemoveListener(GrainChange);
        View.Temperature.onValueChanged.RemoveListener(Temperature);
        View.Tint.onValueChanged.RemoveListener(TintChange);
        View.ChromaticAbberation.onValueChanged.RemoveListener(ChromaticAbberationChange);
    }

    public override void OnRegister()
    {
        base.OnRegister();
        Init();
        View.CloseButton.onClick.AddListener(Close);
        View.Vignette.onValueChanged.AddListener(VignetteChange);
        View.Bloom.onValueChanged.AddListener(BloomChange);
        View.Distortion.onValueChanged.AddListener(DistortionChange);
        View.Grain.onValueChanged.AddListener(GrainChange);
        View.Temperature.onValueChanged.AddListener(Temperature);
        View.Tint.onValueChanged.AddListener(TintChange);
        View.ChromaticAbberation.onValueChanged.AddListener(ChromaticAbberationChange);
        View.ColorGradingMode.onValueChanged.AddListener(ColorGradingModeChange);
    }

    private void ColorGradingModeChange(int arg0)
    {
        switch (arg0)
        {
            case 1:
                View.Volume.profile.GetSetting<ColorGrading>().tonemapper.value = Tonemapper.ACES;
                break;
            case 2:
                View.Volume.profile.GetSetting<ColorGrading>().tonemapper.value = Tonemapper.Neutral;
                break;
            default:
                View.Volume.profile.GetSetting<ColorGrading>().tonemapper.value = Tonemapper.None;
                break;
        }
    }

    private void ChromaticAbberationChange(float arg0)
    {
        View.Volume.profile.GetSetting<ChromaticAberration>().intensity.value = arg0;
    }

    private void TintChange(float arg0)
    {
        View.Volume.profile.GetSetting<ColorGrading>().tint.value = arg0;
    }

    private void Temperature(float arg0)
    {
        View.Volume.profile.GetSetting<ColorGrading>().temperature.value = arg0;
    }

    private void GrainChange(float arg0)
    {
        View.Volume.profile.GetSetting<Grain>().intensity.value = arg0;
    }

    private void DistortionChange(float arg0)
    {
        View.Volume.profile.GetSetting<LensDistortion>().intensity.value = arg0;
    }

    private void BloomChange(float arg0)
    {
        View.Volume.profile.GetSetting<Bloom>().intensity.value = arg0;
    }

    private void VignetteChange(float arg0)
    {
        View.Volume.profile.GetSetting<Vignette>().intensity.value = arg0;
    }

    private void Init()
    {
        View.Volume = ReferenceManager.Instance.PostProcessVolume;
        View.Vignette.value = View.Volume.profile.GetSetting<Vignette>().intensity.value;
        View.ChromaticAbberation.value = View.Volume.profile.GetSetting<ChromaticAberration>().intensity.value;
        View.Bloom.value = View.Volume.profile.GetSetting<Bloom>().intensity.value;
        View.Temperature.value = View.Volume.profile.GetSetting<ColorGrading>().temperature.value;
        View.Tint.value = View.Volume.profile.GetSetting<ColorGrading>().tint.value;
        View.Distortion.value = View.Volume.profile.GetSetting<LensDistortion>().intensity.value;
        View.Grain.value = View.Volume.profile.GetSetting<Grain>().intensity.value;
        View.ColorGradingMode.value = 0;
    }
    

    private void Close()
    {
        Destroy(gameObject);
    }
}
