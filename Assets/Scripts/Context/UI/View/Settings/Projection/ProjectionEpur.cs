using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ProjectionEpur : MonoBehaviour
{
    [SerializeField] public Epur Epur;
    private Button Button;

    private void Awake()
    {
        Button = GetComponent<Button>();
        StartCoroutine(DelayedAwake());
    }

    private IEnumerator DelayedAwake()
    {
        yield return new WaitForSeconds(0.1f);
        Button.onClick.AddListener(Call);
    }

    private void Update()
    {
        switch (Epur)
        {
            case Epur.Facade:
                if (Input.GetKeyDown(KeyCode.DownArrow))
                {
                    Call();
                }
                break;
            case Epur.Plan:
                if (Input.GetKeyDown(KeyCode.RightArrow))
                {
                    Call();
                }
                break;
            case Epur.Profile:
                if (Input.GetKeyDown(KeyCode.UpArrow))
                {
                    Call();
                }
                break;
        }
    }

    private void Call()
    {
        BackendManager.Instance.CameraMovementView.EpurMovement(Epur);
    }
}
