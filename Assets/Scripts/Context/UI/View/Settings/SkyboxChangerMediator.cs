using System.Collections;
using System.Collections.Generic;
using strange.extensions.mediation.impl;
using TMPro;
using UnityEngine;

public class SkyboxChangerMediator : Mediator
{
    [Inject] public SkyboxChangerView View { get; set; }
    private TMP_Dropdown Dropdown;
    
    public override void OnRegister()
    {
        base.OnRegister();
        Init();
    }

    public override void OnRemove()
    {
        base.OnRemove();
        Dropdown.onValueChanged.RemoveListener(SkyboxChange);
    }

    private void Init()
    {
        Dropdown = GetComponent<TMP_Dropdown>();
        Dropdown.ClearOptions();
        foreach (var item in ReferenceManager.Instance.Skyboxes)
        {
            Dropdown.options.Add(new TMP_Dropdown.OptionData()
            {
                text = item.ID
            });
        }
        Dropdown.onValueChanged.AddListener(SkyboxChange);
    }

    private void SkyboxChange(int arg0)
    {
        RenderSettings.skybox = ReferenceManager.Instance.Skyboxes[arg0].Material;
    }
}
