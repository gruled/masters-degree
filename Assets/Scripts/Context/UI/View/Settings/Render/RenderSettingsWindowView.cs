using System.Collections;
using System.Collections.Generic;
using Michsky.UI.ModernUIPack;
using strange.extensions.mediation.impl;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class RenderSettingsWindowView : View
{
    [SerializeField] public Button CloseButton;
    [SerializeField] public SliderManager ScaleSlider;
    [SerializeField] public Button RenderButton;
    [SerializeField] public TMP_Dropdown ShadowsType;
    [SerializeField] public TMP_Dropdown AntialiasingType;
}
