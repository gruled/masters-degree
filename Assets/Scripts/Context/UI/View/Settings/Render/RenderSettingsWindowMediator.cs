using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using System.Runtime.InteropServices;
using Dummiesman;
using strange.extensions.mediation.impl;
using TMPro;
using UnityEngine;
using UnityEngine.Rendering.PostProcessing;

public class RenderSettingsWindowMediator : Mediator
{
    [DllImport("__Internal")]
    private static extern void SaveRender(byte[] array, int byteLength, string fileName);
    
    [Inject] public RenderSettingsWindowView View { get; set; }
    private float Scale;

    public override void OnRemove()
    {
        base.OnRemove();
        View.CloseButton.onClick.RemoveListener(Close);
        View.ScaleSlider.onValueChanged.RemoveListener(ScaleChange);
        View.RenderButton.onClick.RemoveListener(RenderStart);
        // PlaygroundManager.Instance.DirectionalLight.shadows.
    }

    public override void OnRegister()
    {
        base.OnRegister();
        Init();
        View.CloseButton.onClick.AddListener(Close);
        View.ScaleSlider.onValueChanged.AddListener(ScaleChange);
        View.RenderButton.onClick.AddListener(RenderStart);
    }

    private void RenderStart()
    {
        StartCoroutine(RenderCoroutine());
    }

    private IEnumerator RenderCoroutine()
    {
        LightShadows currentShadows = PlaygroundManager.Instance.DirectionalLight.shadows;
        PostProcessLayer.Antialiasing currentAntialiasing =
            ReferenceManager.Instance.Camera.GetComponent<PostProcessLayer>().antialiasingMode;
        switch (View.ShadowsType.value)
        {
            case 0:
                PlaygroundManager.Instance.DirectionalLight.shadows = LightShadows.None;
                break;
            case 1:
                PlaygroundManager.Instance.DirectionalLight.shadows = LightShadows.Hard;
                break;
            case 2:
                PlaygroundManager.Instance.DirectionalLight.shadows = LightShadows.Soft;
                break;
        }

        switch (View.AntialiasingType.value)
        {
            case 0:
                ReferenceManager.Instance.Camera.GetComponent<PostProcessLayer>().antialiasingMode =
                    PostProcessLayer.Antialiasing.None;
                break;
            case 1:
                ReferenceManager.Instance.Camera.GetComponent<PostProcessLayer>().antialiasingMode =
                    PostProcessLayer.Antialiasing.FastApproximateAntialiasing;
                break;
            case 2:
                ReferenceManager.Instance.Camera.GetComponent<PostProcessLayer>().antialiasingMode =
                    PostProcessLayer.Antialiasing.SubpixelMorphologicalAntialiasing;
                break;
            case 3:
                ReferenceManager.Instance.Camera.GetComponent<PostProcessLayer>().antialiasingMode =
                    PostProcessLayer.Antialiasing.TemporalAntialiasing;
                break;
        }

        UIManager.Instance.GetComponent<Canvas>().enabled = false;
        yield return new WaitForEndOfFrame();
        Texture2D screen = ScreenCapture.CaptureScreenshotAsTexture((int) View.ScaleSlider.mainSlider.value);
        UIManager.Instance.GetComponent<Canvas>().enabled = true;
        byte[] textureBytes = screen.EncodeToPNG();
        SaveRender(textureBytes, textureBytes.Length, "Render.png");
        PlaygroundManager.Instance.DirectionalLight.shadows = currentShadows;
        ReferenceManager.Instance.Camera.GetComponent<PostProcessLayer>().antialiasingMode = currentAntialiasing;
    }

    private void ScaleChange(float arg0)
    {
        Scale = arg0;
    }

    private void Close()
    {
        GameObject.Destroy(gameObject);
    }

    private void Init()
    {
        Scale = View.ScaleSlider.mainSlider.value;
    }
}
