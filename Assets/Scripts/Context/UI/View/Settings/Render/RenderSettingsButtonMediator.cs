using System.Collections;
using System.Collections.Generic;
using System.Linq;
using strange.extensions.mediation.impl;
using UnityEngine;
using UnityEngine.UI;

public class RenderSettingsButtonMediator : Mediator
{
    [Inject] public RenderSettingsButtonView View { get; set; }

    public override void OnRemove()
    {
        base.OnRemove();
        View.Button.onClick.RemoveListener(OpenRenderWindow);
    }
    
    public override void OnRegister()
    {
        base.OnRegister();
        Init();
        View.Button.onClick.AddListener(OpenRenderWindow);
    }

    private void OpenRenderWindow()
    {
        GameObjectReference reference;
        reference = ReferenceManager.Instance.References.First(x => x.Type.Equals(ReferenceType.RenderingWindow));
        reference.Reference.InstantiateAsync(UIManager.Instance.UIMainView.FrameableWindow.transform);
    }

    private void Init()
    {
        View.Button = GetComponent<Button>();
    }
}
