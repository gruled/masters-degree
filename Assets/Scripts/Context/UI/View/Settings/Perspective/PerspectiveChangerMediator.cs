using System.Collections;
using System.Collections.Generic;
using strange.extensions.mediation.impl;
using UnityEngine;
using UnityEngine.UI;

public class PerspectiveChangerMediator : Mediator
{
    [SerializeField] public PerspectiveChangerView View { get; set; }
    private Toggle Toggle;

    public override void OnRemove()
    {
        base.OnRemove();
        Toggle.onValueChanged.RemoveListener(PerspectiveChange);
    }

    public override void OnRegister()
    {
        base.OnRegister();
        Init();
        Toggle.onValueChanged.AddListener(PerspectiveChange);
    }

    private void PerspectiveChange(bool arg0)
    {
        ReferenceManager.Instance.Camera.orthographic = !arg0;
    }

    private void Init()
    {
        Toggle = GetComponent<Toggle>();
    }
}
