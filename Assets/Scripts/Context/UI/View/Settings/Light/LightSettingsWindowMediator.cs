using System.Collections;
using System.Collections.Generic;
using strange.extensions.mediation.impl;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.Rendering.PostProcessing;

public class LightSettingsWindowMediator : Mediator
{
    [Inject] public LightSettingsWindowView View { get; set; }
    private Light _light;

    public override void OnRemove()
    {
        base.OnRemove();
        View.Intensity.mainSlider.onValueChanged.RemoveListener(IntensityChange);
        View.CloseButton.onClick.RemoveListener(Close);
        View.ShadowsType.onValueChanged.RemoveListener(ShadowChange);
        View.RotationX.onValueChanged.RemoveListener(RotationXChange);
        View.RotationY.onValueChanged.RemoveListener(RotationYChange);
        View.RotationZ.onValueChanged.RemoveListener(RotationZChange);
    }

    public override void OnRegister()
    {
        base.OnRegister();
        Init();
        View.Intensity.mainSlider.onValueChanged.AddListener(IntensityChange);
        View.CloseButton.onClick.AddListener(Close);
        View.ShadowsType.onValueChanged.AddListener(ShadowChange);
        View.RotationX.onValueChanged.AddListener(RotationXChange);
        View.RotationY.onValueChanged.AddListener(RotationYChange);
        View.RotationZ.onValueChanged.AddListener(RotationZChange);
    }

    private void RotationZChange(string arg0)
    {
        _light.transform.rotation = Quaternion.Euler(_light.transform.rotation.eulerAngles.x,
            _light.transform.rotation.eulerAngles.y,
            float.Parse(arg0));
    }

    private void RotationYChange(string arg0)
    {
        _light.transform.rotation = Quaternion.Euler(_light.transform.rotation.eulerAngles.x, float.Parse(arg0),
            _light.transform.rotation.eulerAngles.z);
    }

    private void RotationXChange(string arg0)
    {
        _light.transform.rotation = Quaternion.Euler(float.Parse(arg0), _light.transform.rotation.eulerAngles.y,
            _light.transform.rotation.eulerAngles.z);
    }

    private void ShadowChange(int arg0)
    {
        switch (arg0)
        {
            case 0:
                PlaygroundManager.Instance.DirectionalLight.shadows = LightShadows.None;
                break;
            case 1:
                PlaygroundManager.Instance.DirectionalLight.shadows = LightShadows.Hard;
                break;
            case 2:
                PlaygroundManager.Instance.DirectionalLight.shadows = LightShadows.Soft;
                break;
        }
    }

    private void Close()
    {
        GameObject.Destroy(gameObject);
    }

    private void IntensityChange(float arg0)
    {
        _light.intensity = arg0 * 0.1f;
    }

    private void Init()
    {
        _light = PlaygroundManager.Instance.DirectionalLight;
        View.Intensity.mainSlider.value = _light.intensity;
        View.ShadowsType.value = (int)_light.shadows;
        View.RotationX.text = _light.transform.rotation.eulerAngles.x.ToString();
        View.RotationY.text = _light.transform.rotation.eulerAngles.y.ToString();
        View.RotationZ.text = _light.transform.rotation.eulerAngles.z.ToString();
    }
}
