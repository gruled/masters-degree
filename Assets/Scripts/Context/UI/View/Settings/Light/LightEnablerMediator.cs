using System.Collections;
using System.Collections.Generic;
using strange.extensions.mediation.impl;
using UnityEngine;
using UnityEngine.UI;

public class LightEnablerMediator : Mediator
{
    [Inject] public LightEnablerView View { get; set; }
    private Toggle Toggle;

    public override void OnRegister()
    {
        base.OnRegister();
        Init();
        Toggle.onValueChanged.AddListener(ToggleChange);
    }

    public override void OnRemove()
    {
        base.OnRemove();
        Toggle.onValueChanged.RemoveListener(ToggleChange);
    }

    private void Init()
    {
        Toggle = this.GetComponent<Toggle>();
        Toggle.isOn = false;
        PlaygroundManager.Instance.DirectionalLight.enabled = false;
    }

    private void ToggleChange(bool arg0)
    {
        PlaygroundManager.Instance.DirectionalLight.enabled = arg0;
    }
}
