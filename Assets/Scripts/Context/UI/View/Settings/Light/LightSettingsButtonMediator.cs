using System.Collections;
using System.Collections.Generic;
using System.Linq;
using strange.extensions.mediation.impl;
using UnityEngine;
using UnityEngine.UI;

public class LightSettingsButtonMediator : Mediator
{
    [Inject] public LightSettingsButtonView View { get; set; }

    public override void OnRemove()
    {
        base.OnRemove();
        View.Button.onClick.RemoveListener(OpenLightningWindow);
    }

    public override void OnRegister()
    {
        base.OnRegister();
        Init();
        View.Button.onClick.AddListener(OpenLightningWindow);
    }

    private void OpenLightningWindow()
    {
        GameObjectReference reference;
        reference = ReferenceManager.Instance.References.First(x => x.Type.Equals(ReferenceType.LightningSettingsWindow));
        reference.Reference.InstantiateAsync(UIManager.Instance.UIMainView.FrameableWindow.transform);
    }

    private void Init()
    {
        View.Button = GetComponent<Button>();
    }
}
