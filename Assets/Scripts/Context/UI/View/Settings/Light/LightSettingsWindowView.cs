using System.Collections;
using System.Collections.Generic;
using Michsky.UI.ModernUIPack;
using strange.extensions.mediation.impl;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class LightSettingsWindowView : View
{
    [SerializeField] public Button CloseButton;
    [SerializeField] public SliderManager Intensity;
    [SerializeField] public TMP_Dropdown ShadowsType;
    [SerializeField] public TMP_InputField RotationX;
    [SerializeField] public TMP_InputField RotationY;
    [SerializeField] public TMP_InputField RotationZ;
    [SerializeField] public FlexibleColorPicker ColorPicker;
}
