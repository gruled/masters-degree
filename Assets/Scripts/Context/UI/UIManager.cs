using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using ProceduralPrimitivesUtil;
using UnityEngine;

public class UIManager : MonoBehaviour
{
    public static UIManager Instance;

    public UIMainView UIMainView;
    public static event Action OnLoad;
    
    private void Awake()
    {
        if (Instance==null)
        {
            Instance = this;
        }
        else if(Instance==this)
        {
            Destroy(gameObject);
        }
        Init();
    }

    private void Init()
    {
        OnLoad?.Invoke();
        UIMainView = GetComponent<UIMainView>();
    }
    

    public void UnHighlightHierarchyElement()
    {
        if (UIMainView.HighlightedElement)
        {
            Color c = UIMainView.HighlightedElement.Background.color;
            UIMainView.HighlightedElement.Background.color = new Color(c.r, c.g, c.b, 0.0f);
            UIMainView.HighlightedElement = null;
        }
    }

    public void HighlightHierarchyElement(HierarchyElementView elementView)
    {
        UIMainView.HighlightedElement = elementView;
        Color c = UIMainView.HighlightedElement.Background.color;
        UIMainView.HighlightedElement.Background.color = new Color(c.r, c.g, c.b, 0.195f);
    }
    
    

    public void CreatePrimitiveSettingsPanel(ProceduralPrimitiveType type)
    {
        ClearPrimitiveSettingsPanel();
        PlaygroundManager.Instance.UnHighlightObject();
        GameObjectReference reference = null;
        switch (type)
        {
            case ProceduralPrimitiveType.Box:
                reference =
                    ReferenceManager.Instance.References.First(x => x.Type == ReferenceType.BoxSettings);
                break;
            case ProceduralPrimitiveType.Cone:
                reference =
                    ReferenceManager.Instance.References.First(x => x.Type == ReferenceType.ConeSettings);
                break;
            case ProceduralPrimitiveType.Ring:
                reference =
                    ReferenceManager.Instance.References.First(x => x.Type == ReferenceType.RingSettings);
                break;
            case ProceduralPrimitiveType.RectTube:
                reference =
                    ReferenceManager.Instance.References.First(x => x.Type == ReferenceType.RectTubeSettings);
                break;
            case ProceduralPrimitiveType.Cylinder:
                reference =
                    ReferenceManager.Instance.References.First(x => x.Type == ReferenceType.CylinderSettings);
                break;
            case ProceduralPrimitiveType.Sphere:
                reference =
                    ReferenceManager.Instance.References.First(x => x.Type == ReferenceType.SphereSettings);
                break;
            case ProceduralPrimitiveType.Pyramid:
                reference =
                    ReferenceManager.Instance.References.First(x => x.Type == ReferenceType.PyramidSettings);
                break;
            case ProceduralPrimitiveType.Torus:
                reference =
                    ReferenceManager.Instance.References.First(x => x.Type == ReferenceType.TorusSettings);
                break;
            default:
                reference =
                    ReferenceManager.Instance.References.First(x => x.Type == ReferenceType.SyntheticSettings);
                break;
                
        }
        reference.Reference.InstantiateAsync(UIMainView.PrimitivePanelView.transform);
    }

    public void ClearPrimitiveSettingsPanel()
    {
        IPrimitiveSettings[] primitiveSettingsArray =
            UIMainView.PrimitivePanelView.GetComponentsInChildren<IPrimitiveSettings>();
        for (int i = 0; i < primitiveSettingsArray.Length; i++)
        {
            GameObject.Destroy((primitiveSettingsArray[i] as PrimitiveSettingsView).gameObject);
        }
    }
}
