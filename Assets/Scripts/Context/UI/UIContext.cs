using System.Collections;
using System.Collections.Generic;
using ProceduralPrimitivesUtil;
using strange.extensions.command.api;
using strange.extensions.command.impl;
using strange.extensions.context.impl;
using UnityEngine;

public class UIContext : MVCSContext
{
    public UIContext(MonoBehaviour view) : base(view)
    {
    }

    protected override void addCoreComponents()
    {
        base.addCoreComponents();
        injectionBinder.Unbind<ICommandBinder>();
        injectionBinder.Bind<ICommandBinder>().To<SignalCommandBinder>().ToSingleton();
    }

    protected override void mapBindings()
    {
        base.mapBindings();
        mediationBinder.BindView<UIMainView>().ToMediator<UIMainMediator>();
        mediationBinder.BindView<PrimitivePanelView>().ToMediator<PrimitivePanelMediator>();
        mediationBinder.BindView<HierarchyElementView>().ToMediator<HierarchyElementMediator>();

        mediationBinder.BindView<BoxSettingsPanelView>().ToMediator<BoxSettingsPanelMediator>();
        mediationBinder.BindView<ConeSettingsPanelView>().ToMediator<ConeSettingsPanelMediator>();
        mediationBinder.BindView<RingSettingsPanelView>().ToMediator<RingSettingsPanelMediator>();
        mediationBinder.BindView<RectTubeSettingsPanelView>().ToMediator<RectTubeSettingsPanelMediator>();
        mediationBinder.BindView<CylinderSettingsPanelView>().ToMediator<CylinderSettingsPanelMediator>();
        mediationBinder.BindView<SphereSettingsPanelView>().ToMediator<SphereSettingsPanelMediator>();
        mediationBinder.BindView<PyramidSettingsPanelView>().ToMediator<PyramidSettingsPanelMediator>();
        mediationBinder.BindView<TorusSettingsPanelView>().ToMediator<TorusSettingsPanelMediator>();
        mediationBinder.BindView<SynthethicSettingsPanelView>().ToMediator<SynthethicSettingsPanelMediator>();
        
        mediationBinder.BindView<SkyboxChangerView>().ToMediator<SkyboxChangerMediator>();
        mediationBinder.BindView<LightEnablerView>().ToMediator<LightEnablerMediator>();
        mediationBinder.BindView<PostprocessingButtonView>().ToMediator<PostprocessingButtonMediator>();
        mediationBinder.BindView<PostProcessingWindowView>().ToMediator<PostProcessingWindowMediator>();

        mediationBinder.BindView<RenderSettingsWindowView>().ToMediator<RenderSettingsWindowMediator>();
        mediationBinder.BindView<RenderSettingsButtonView>().ToMediator<RenderSettingsButtonMediator>();

        mediationBinder.BindView<ImportObjectView>().ToMediator<ImportObjectMediator>();
        mediationBinder.BindView<LightSettingsButtonView>().ToMediator<LightSettingsButtonMediator>();
        mediationBinder.BindView<LightSettingsWindowView>().ToMediator<LightSettingsWindowMediator>();
        mediationBinder.BindView<MeshStatisticsView>().ToMediator<MeshStatisticsMediator>();
        mediationBinder.BindView<PerspectiveChangerView>().ToMediator<PerspectiveChangerMediator>();
    }
}
