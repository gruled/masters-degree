using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using strange.extensions.command.impl;
using UnityEngine;
using UnityEngine.AddressableAssets;

public class LoadPlaygroundCommand : Command
{
    public override void Execute()
    {
        var reference = ReferenceManager.Instance.References.First(x => x.Type.Equals(ReferenceType.Playground));
        reference.Reference.InstantiateAsync();
    }
}