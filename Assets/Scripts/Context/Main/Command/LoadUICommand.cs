using System.Collections;
using System.Collections.Generic;
using System.Linq;
using strange.extensions.command.impl;
using UnityEngine;

public class LoadUICommand : Command
{
    public override void Execute()
    {
        var reference = ReferenceManager.Instance.References.First(x => x.Type.Equals(ReferenceType.UICanvas));
        reference.Reference.InstantiateAsync();
    }
}
