using System.Collections;
using System.Collections.Generic;
using strange.extensions.command.api;
using strange.extensions.command.impl;
using strange.extensions.context.api;
using strange.extensions.context.impl;
using strange.extensions.signal.impl;
using UnityEngine;

public class MainContext : MVCSContext
{
    public MainContext(MonoBehaviour view) : base(view)
    {
    }

    protected override void addCoreComponents()
    {
        base.addCoreComponents();
        injectionBinder.Unbind<ICommandBinder>();
        injectionBinder.Bind<ICommandBinder>().To<SignalCommandBinder>().ToSingleton();
    }

    protected override void mapBindings()
    {
        base.mapBindings();
        commandBinder.Bind<StartSignal>()
            .To<LoadBackendCommand>()
            .To<LoadPlaygroundCommand>()
            .To<LoadUICommand>();

        mediationBinder.BindView<MainView>().ToMediator<MainMediator>();
    }
    
}
