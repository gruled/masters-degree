using System;
using System.Collections;
using System.Collections.Generic;
using strange.extensions.mediation.impl;
using UnityEngine;

public class MainMediator : Mediator
{
    [Inject] public MainView View { get; set; }
    [Inject] public StartSignal StartSignal { get; set; }

    public override void OnRegister()
    {
        base.OnRegister();
        ReferenceManager.OnLoad += LoadManger;
    }

    private void LoadManger()
    {
        StartSignal.Dispatch();
    }
}
