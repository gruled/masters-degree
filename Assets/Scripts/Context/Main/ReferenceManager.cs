using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.Rendering.PostProcessing;

public class ReferenceManager : MonoBehaviour
{
    public static ReferenceManager Instance;
    [SerializeField] public List<GameObjectReference> References;
    [SerializeField] public List<PrimitiveMaterialReference> Materials;
    [SerializeField] public List<SkyboxMaterialReference> Skyboxes;
    [SerializeField] public Camera Camera;
    [SerializeField] public PostProcessVolume PostProcessVolume;
    [SerializeField] public Material NullMaterial;
    public static event Action OnLoad;

    private void Awake()
    {
        if (Instance == null)
        {
            Instance = this;
        }
        else if (Instance == this)
        {
            Destroy(gameObject);
        }

        Init();
    }

    private void Init()
    {
        References = Resources.LoadAll<GameObjectReference>("Configs/").ToList();
        Materials = Resources.LoadAll<PrimitiveMaterialReference>("Configs/PrimitiveMaterials/").ToList();
        Skyboxes = Resources.LoadAll<SkyboxMaterialReference>("Configs/SkyboxMaterials/").ToList();
        OnLoad?.Invoke();
    }
}
