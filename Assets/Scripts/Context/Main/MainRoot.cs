using System;
using System.Collections;
using System.Collections.Generic;
using strange.extensions.context.impl;
using UnityEngine;

public class MainRoot : ContextView
{
    private void Awake()
    {
        this.context = new MainContext(this);
    }
}
