using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.ResourceManagement.AsyncOperations;

public class PlaygroundManager : MonoBehaviour
{
    public static PlaygroundManager Instance;

    [SerializeField] public List<PrimitiveView> Objects;

    [SerializeField] public PrimitiveView PrimaryObject;
    [SerializeField] public Light DirectionalLight;

    private void Awake()
    {
        if (Instance==null)
        {
            Instance = this;
        }
        else if (Instance == this)
        {
            Destroy(gameObject);
        }
        Init();
    }

    private void Init()
    {
        
    }

    public void UnHighlightObject()
    {
        // if (PrimaryObject)
        // {
        //     PrimaryObject.GetComponent<MeshRenderer>().material.SetFloat("_OutlinePower", 0.0f);
        // }
        Debug.Log($"UnHighlight object");
    }
    
    public void HighlightObject()
    {
        // if (PrimaryObject)
        // {
        //     PrimaryObject.GetComponent<MeshRenderer>().material.SetFloat("_OutlinePower", 0.01f);
        // }
        Debug.Log($"Highlight object");
    }

    public void ChangePrimaryObject()
    {
        UIManager.Instance.UnHighlightHierarchyElement();
        UIManager.Instance.CreatePrimitiveSettingsPanel(PrimaryObject.Type);
        StartCoroutine(DelayHighlightElement());
    }

    private IEnumerator DelayHighlightElement()
    {
        yield return new WaitForSeconds(0.3f);
        HierarchyElementView hierarchyElementView = UIManager.Instance.UIMainView.HierarchyPanel
            .GetComponentsInChildren<HierarchyElementView>().First(x => x.ID.Equals(PrimaryObject.ID));
        UIManager.Instance.HighlightHierarchyElement(hierarchyElementView);
    }

    public void AddHierarchyElement()
    {
        StartCoroutine(LoadElement());
    }

    private AsyncOperationHandle<GameObject> handle;

    private bool GetHandleStatus()
    {
        return handle.IsDone;
    }
    
    private IEnumerator LoadElement()
    {
        GameObjectReference reference = ReferenceManager.Instance.References.First(x => x.Type == ReferenceType.HierarchyElement);
        handle = reference.Reference.InstantiateAsync(UIManager.Instance.UIMainView.HierarchyPanel.transform);
        yield return new WaitUntil(GetHandleStatus);
        HierarchyElementView element = handle.Result.GetComponent<HierarchyElementView>();
        element.ID = PrimaryObject.ID;
        element.Name.SetText(PrimaryObject.name);
    }
}
