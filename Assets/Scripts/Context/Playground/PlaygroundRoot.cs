using System;
using System.Collections;
using System.Collections.Generic;
using strange.extensions.context.impl;
using UnityEngine;

public class PlaygroundRoot : ContextView
{
    private void Awake()
    {
        this.context = new PlaygroundContext(this);
    }
}
