using System;
using System.Collections;
using System.Collections.Generic;
using ProceduralPrimitivesUtil;
using strange.extensions.mediation.impl;
using UnityEngine;
using UnityEngine.UI;

public class PlaygroundMediator : Mediator
{
    [Inject] public PlaygroundView View { get; set; }

    private void Update()
    {
        if (Input.GetKey(KeyCode.Space))
        {
            if (Input.GetKeyDown(KeyCode.Mouse0))
            {
                RaycastHit hit;
                Ray ray = ReferenceManager.Instance.Camera.ScreenPointToRay(Input.mousePosition);
                if (Physics.Raycast(ray, out hit))
                {
                    PrimitiveView primitiveView;
                    if (hit.transform.TryGetComponent(out primitiveView))
                    {
                        PlaygroundManager.Instance.UnHighlightObject();
                        PlaygroundManager.Instance.PrimaryObject = primitiveView;
                        PlaygroundManager.Instance.ChangePrimaryObject();
                        PlaygroundManager.Instance.HighlightObject();
                    
                        BackendManager.Instance.SelectObject();
                    }
                }
            }   
        }
    }
}
