using System.Collections;
using System.Collections.Generic;
using ProceduralPrimitivesUtil;
using strange.extensions.mediation.impl;
using UnityEngine;

public class PrimitiveView : View
{
    [SerializeField] public ProceduralPrimitiveType Type;
    [SerializeField] public string ID;
    [SerializeField] public bool IsPrimitive = true;
}
