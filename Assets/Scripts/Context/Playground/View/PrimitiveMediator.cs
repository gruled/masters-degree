using System.Collections;
using System.Collections.Generic;
using strange.extensions.mediation.impl;
using UnityEngine;

public class PrimitiveMediator : Mediator
{
    [Inject] public PrimitiveView View { get; set; }
}
