using System;
using System.Collections;
using System.Collections.Generic;
using ProceduralPrimitivesUtil;
using UnityEngine;

public class StatisticsManager : MonoBehaviour
{
    public static StatisticsManager Instance;

    private Dictionary<ProceduralPrimitiveType, int> PrimitiveCounter;

    private void Awake()
    {
        if (Instance==null)
        {
            Instance = this;
        }
        else if (Instance==null)
        {
            Destroy(gameObject);
        }
        Init();
    }

    private void Init()
    {
        PrimitiveCounter = new Dictionary<ProceduralPrimitiveType, int>();
    }

    public int GetPrimitiveCounterByType(ProceduralPrimitiveType type)
    {
        if (PrimitiveCounter.ContainsKey(type))
        {
            return ++PrimitiveCounter[type];
        }
        PrimitiveCounter.Add(type, 0);
        return 0;
    }

    public void ResetPrimitiveCounter()
    {
        PrimitiveCounter.Clear();
    }
}
