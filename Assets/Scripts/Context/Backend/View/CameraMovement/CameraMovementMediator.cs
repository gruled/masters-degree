using System;
using System.Collections;
using System.Collections.Generic;
using strange.extensions.mediation.impl;
using UnityEngine;
using UnityEngine.UIElements;

public class CameraMovementMediator : Mediator
{
    [Inject] public CameraMovementView View { get; set; }
    private Camera CurrentCamera;
    private Vector2 scrollDelta;
    private Vector2 lookDelta;
    private Vector3 lastPosition;
    private Vector3 direction;
    private Vector3 mousePosition;

    public override void OnRemove()
    {
        base.OnRemove();
        View.OnEpurMovement -= EpurMovement;
        BackendManager.Instance.OnSelectObject -= SelectObject;
    }

    public override void OnRegister()
    {
        base.OnRegister();
        StartCoroutine(_delayedAwake());
        View.OnEpurMovement += EpurMovement;
    }

    private void EpurMovement(Epur obj)
    {
        switch (obj)
        {
            case Epur.Facade:
                CurrentCamera.transform.rotation = Quaternion.Euler(0, 0, 0);
                CurrentCamera.transform.position = new Vector3(0, 0, -5);
                break;
            case Epur.Plan:
                CurrentCamera.transform.rotation = Quaternion.Euler(0, -90, 0);
                CurrentCamera.transform.position = new Vector3(5, 0, 0);
                break;
            case Epur.Profile:
                CurrentCamera.transform.rotation = Quaternion.Euler(90, 0, 0);
                CurrentCamera.transform.position = new Vector3(0, 5, 0);
                break;
        }
    }

    private IEnumerator _delayedAwake()
    {
        yield return new WaitForSeconds(0.1f);
        BackendManager.Instance.OnSelectObject += SelectObject;
    }

    private void SelectObject()
    {
        CurrentCamera.transform.LookAt(PlaygroundManager.Instance.PrimaryObject.transform);
    }

    private void Awake()
    {
        CurrentCamera = ReferenceManager.Instance.Camera;
    }

    private void Update()
    {
        // Scroll();
        // Look();
    }

    private void Look()
    {
        if (Input.GetMouseButton(1))
        {
            if (PlaygroundManager.Instance.PrimaryObject)
            {
                mousePosition = Input.mousePosition;
                direction = (mousePosition - lastPosition).normalized;
                direction.Set(-direction.y, direction.x, 0);
                // CurrentCamera.transform.RotateAround(PlaygroundManager.Instance.PrimaryObject.transform.position,
                //     direction, View.LookSpeed);
                CurrentCamera.transform.RotateAround(PlaygroundManager.Instance.PrimaryObject.transform.position,
                    direction, View.LookSpeed);
                CurrentCamera.transform.rotation = Quaternion.Euler(CurrentCamera.transform.rotation.eulerAngles.x,
                    CurrentCamera.transform.rotation.eulerAngles.y, 0);
                lastPosition = mousePosition;
            }
        }
    }

    private void Scroll()
    {
        scrollDelta = Input.mouseScrollDelta;
        CurrentCamera.transform.Translate(new Vector3(0, 0, scrollDelta.y * View.ScrollSpeed), Space.Self);
    }
}
