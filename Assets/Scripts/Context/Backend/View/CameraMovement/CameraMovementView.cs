using System;
using System.Collections;
using System.Collections.Generic;
using strange.extensions.mediation.impl;
using UnityEngine;

public class CameraMovementView : View
{ 
    [Min(0)][SerializeField] public float ScrollSpeed;
    [Min(0)] [SerializeField] public float LookSpeed;

    public event Action<Epur> OnEpurMovement;
    public void EpurMovement(Epur epur)
    {
        OnEpurMovement?.Invoke(epur);
    }
}

public enum Epur
{
    Facade = 1,
    Plan = 2,
    Profile = 3
}
