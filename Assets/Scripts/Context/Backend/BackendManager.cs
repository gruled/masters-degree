using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BackendManager : MonoBehaviour
{
    public static BackendManager Instance;
    [SerializeField] public CameraMovementView CameraMovementView;
    
    public event Action OnSelectObject;

    public static event Action OnLoad;
    
    private void Awake()
    {
        if (Instance != null && Instance != this)
        {
            Destroy(gameObject);
            return;
        }

        Instance = this;
    }

    public void SelectObject()
    {
        OnSelectObject?.Invoke();
    }
    
}
