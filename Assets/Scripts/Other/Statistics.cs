using System;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using Unity.Profiling;
using UnityEngine;

public class Statistics : MonoBehaviour
{
    [SerializeField] private TextMeshProUGUI FPS;
    [SerializeField] private TextMeshProUGUI Triangles;
    [SerializeField] private TextMeshProUGUI Vertices;
    private ProfilerRecorder TrianglesRecorder;
    private ProfilerRecorder VerticesRecorder;
    

    private void Awake()
    {
        Init();
        StartCoroutine(Upd());
    }

    private IEnumerator Upd()
    {
        while (true)
        {
            yield return new WaitForSeconds(0.5f);
            FPS.SetText((1.0f/Time.deltaTime).ToString());
            Triangles.SetText(TrianglesRecorder.LastValue.ToString());
            Vertices.SetText(VerticesRecorder.LastValue.ToString());
        }
    }

    private void Init()
    {
        FPS?.SetText("");
        Triangles?.SetText("");
        Vertices?.SetText("");
        TrianglesRecorder = ProfilerRecorder.StartNew(ProfilerCategory.Render, "Triangles Count");
        VerticesRecorder = ProfilerRecorder.StartNew(ProfilerCategory.Render, "Vertices Count");
    }
}
