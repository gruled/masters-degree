using System.Collections;
using System.Collections.Generic;
using strange.extensions.mediation.impl;
using UnityEngine;


[CreateAssetMenu(fileName = "SkyboxMaterialReference", menuName = "ScriptableObjects/SkyboxMaterialReference")]
public class SkyboxMaterialReference : ScriptableObject
{
    [SerializeField] private string id;
    [SerializeField] private Material _material;
    
    public string ID => id;
    public Material Material => _material;
}
