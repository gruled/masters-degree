using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AddressableAssets;


[CreateAssetMenu(fileName = "GameObjectReference", menuName = "ScriptableObjects/GameObjectReference")]
public class GameObjectReference : ScriptableObject
{
    [SerializeField] private string id;
    [SerializeField] private AssetReference reference;
    [SerializeField] private ReferenceType type;
    
    public string ID => id;
    public AssetReference Reference => reference;

    public ReferenceType Type => type;
}

public enum ReferenceType
{
    Playground = 1,
    UICanvas = 2,
    PrimitiveShader = 3,
    PrimitiveMaterial = 4,
    BoxSettings = 5,
    HierarchyElement = 6,
    Backend = 7,
    ConeSettings = 8,
    RingSettings = 9,
    RectTubeSettings = 10,
    CylinderSettings = 11,
    SphereSettings = 12,
    PyramidSettings = 13,
    TorusSettings = 14,
    PostProcessingWindow = 15,
    RenderingWindow = 16,
    SyntheticSettings = 17,
    LightningSettingsWindow = 18
}
