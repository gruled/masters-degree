using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "PrimitiveMaterialReference", menuName = "ScriptableObjects/PrimitiveMaterialReference")]
public class PrimitiveMaterialReference : ScriptableObject
{
    [SerializeField] private string id;
    [SerializeField] private Texture2D albedo;
    [SerializeField] private Texture2D normal;
    
    public string ID => id;
    public Texture2D Albdeo => albedo;
    public Texture2D Normal => normal;
}
